package com.epam.training.android;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class GoogleSearch extends Activity {

	private static final int PARAMETER_INDEX_ASYNCTASK_SEARCH_TERM = 0;
	
	private static final String SEARCH_URL = "http://ajax.googleapis.com/ajax/services/search/web?v=1.0&rsz=large&q=";
	
	private EditText searchTerm;
	private Button searchButton;
	private ListView searchResultListView;

	private AlertDialog emptySearchTermAlertDialog = null;
	private ProgressDialog searchInProgressDialog;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		searchTerm = (EditText) findViewById(R.id.searchTerm);
		searchButton = (Button) findViewById(R.id.searchButton);
		searchResultListView = (ListView) findViewById(R.id.searchResultList);

		searchInProgressDialog = new ProgressDialog(this);
		searchInProgressDialog.setTitle(R.string.searchInProgressDialogTitle);
		searchInProgressDialog.setCancelable(false);
		
		
		// Handling web search intent.
		Intent intent = getIntent();
		if (Intent.ACTION_WEB_SEARCH.equals(intent.getAction())) {
			String intentSearchTerm = intent
					.getStringExtra(SearchManager.QUERY);
			searchTerm.setText(intentSearchTerm);
			doSearchAndShowResult(intentSearchTerm);
		}

		// Starting search when pressing Enter key.
		searchTerm.setOnKeyListener(new View.OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// I'm still a really hungry onKeyListener!
				boolean keyEventNeedToBeConsumed = true;
				
				switch (keyCode) {
					case KeyEvent.KEYCODE_ENTER:
						String searchTermValue = searchTerm.getText().toString();
						doSearchAndShowResult(searchTermValue);
						break;
					case KeyEvent.KEYCODE_MENU:
					case KeyEvent.KEYCODE_BACK:
						// We can't return true always because if we consume Menu press
						// the onCreateOptionsMenu() won't be invoked.
						keyEventNeedToBeConsumed = false;
				}

				return keyEventNeedToBeConsumed;
			}
		});

		// Attaching onclick listener to Search button.
		searchButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View view) {
				String searchTermValue = searchTerm.getText().toString();
				doSearchAndShowResult(searchTermValue);
			}
		});

		// Attaching context menu to result list.
		registerForContextMenu(searchResultListView);
	}
	
	/**
	 * Does the whole search and populates the result list.
	 * 
	 * @param searchTermString
	 *            A very interesting text you want to know more about.
	 */
	private void doSearchAndShowResult(String searchTermString) {
			if (searchTermString.length() > 0) {
				searchInProgressDialog.show();
				// All the search is done by the AsyncTask now.
				SearchTask searchTask = new SearchTask();
				searchTask.execute(new String[] {searchTermString});
			} else {
				if (emptySearchTermAlertDialog == null) {
					AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(
							GoogleSearch.this);
					dialogBuilder
							.setMessage(R.string.emptySearchTermAlertDialogText)
							.setCancelable(true) // It is cancelable
													// with phone Back
													// button
							.setNegativeButton(
									R.string.emptySearchTermAlertDialogButtonText,
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											dialog.cancel();
										}
									});
					emptySearchTermAlertDialog = dialogBuilder.create();
				}
				emptySearchTermAlertDialog.show();
			}


		// Selecting all the previously searched text to make new searches
		// easier.
		searchTerm.requestFocus();
		searchTerm.selectAll();
	}

	/**
	 * Does the google search in the background.
	 *
	 */
	private class SearchTask extends AsyncTask<String, Void, List<HashMap<String, String>>> {

		@Override
		protected List<HashMap<String, String>> doInBackground(String... parameters) {
			List<HashMap<String, String>> searchResultItemList = new ArrayList<HashMap<String, String>>();
			try {
				searchResultItemList = doSearch(parameters[PARAMETER_INDEX_ASYNCTASK_SEARCH_TERM]);
			} catch (Exception e) {
				// Booo...
				e.printStackTrace();
			}
			return searchResultItemList;
		}
		
		@Override
		/**
		 * Takes the result from doInBackground() and refreshes UI.
		 */
		protected void onPostExecute(List<HashMap<String, String>> searchResultItemList) {
			super.onPostExecute(searchResultItemList);
			searchInProgressDialog.dismiss();
			showResults(searchResultItemList);
		}

		/**
		 * Contains all the non-UI-related tasks from the former doSearchAndShowResults() method.
		 * @param searchTermString
		 * @throws JSONException
		 * @throws UnsupportedEncodingException
		 */
		private List<HashMap<String, String>> doSearch(String searchTermString) throws JSONException, UnsupportedEncodingException {
			List<HashMap<String, String>> searchResultItemList = new ArrayList<HashMap<String, String>>();
			
			// Making it more url-friendly.
			String fullSearchUrl = SEARCH_URL
					+ URLEncoder.encode(searchTermString, "UTF-8");
			JSONObject fullResponse = new JSONObject(
					getSearchResultAsJson(fullSearchUrl));

			JSONObject responseData = fullResponse
					.getJSONObject("responseData");
			JSONArray results = responseData.getJSONArray("results");

			// In the beginning was the ... List?!
			searchResultItemList.clear();

			// Iterating over results.
			for (int i = 0; i < results.length(); i++) {
				JSONObject resultItem = results.getJSONObject(i);

				// Collecting search results and building of this
				// wunderchön list-of-map drives our ListView
				HashMap<String, String> resultItemMap = new HashMap<String, String>();

				// Title of result.
				resultItemMap.put("titleNoFormatting",
						resultItem.getString("titleNoFormatting"));

				// Smart and short version of the target url.
				resultItemMap.put("visibleUrl",
						resultItem.getString("visibleUrl"));

				// Long description of the result. Contains some
				// HTML elements like <b>..</b> but no worries.
				// A bit ugly but works so... put here a TO-DO and
				// I'm going to fix it later...or after that.
				// TODO remove/convert HTML elements from
				// description.
				resultItemMap.put("content",
						resultItem.getString("content"));

				// Full url of the target page.
				resultItemMap.put("url", resultItem.getString("url"));

				searchResultItemList.add(resultItemMap);
			}
			
			return searchResultItemList;
		}
		
		/**
		 * Searches text on Google and returns the result in JSON format.
		 * 
		 * @param searchTerm
		 *            Expression to search.
		 * @return Result list as JSON.
		 */
		private String getSearchResultAsJson(String searchTerm) {
			StringBuilder builder = new StringBuilder();
			HttpClient client = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(searchTerm);
			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
				} else {
					handleGeneralError("Failed to download file");
				}
			} catch (ClientProtocolException e) {
				handleGeneralError(e.toString());
			} catch (IOException e) {
				handleGeneralError(e.toString());
			} catch (Exception e) {
				handleGeneralError(e.toString());
			}
			return builder.toString();
		}
		
		/**
		 * All the UI-related code (untouched) from the former doSearchAndShowResults()
		 */
		private void showResults(final List<HashMap<String, String>> searchResultItemList) {
			// Utilizing some child of android.widget.BaseAdapter,
			// in this case SimpleAdapter,
			// to provide data for ListView.
			SimpleAdapter searchResultListAdapter = new SimpleAdapter(
					GoogleSearch.this, // Context for our ListView
					searchResultItemList, // List-of-maps we built
											// up above.
					R.layout.search_result_item, // Name of the
													// layout to
													// display one
													// item.
					new String[] { "content", "visibleUrl",
							"titleNoFormatting" }, // Keys of the
													// map in this
													// list...
					new int[] { R.id.searchResultItemContent,
							R.id.searchResultVisibleUrl,
							R.id.searchResultItemTitle }); // ...and
															// view
															// IDs...
			// ...these two rows maps data to view. "content" will
			// be displayed in R.id.searchResultItemContent etc...

			// Don't forget to assign it to the list to give meaning
			// to the code above.
			searchResultListView.setAdapter(searchResultListAdapter);

			searchResultListView
					.setOnItemClickListener(new OnItemClickListener() {
						public void onItemClick(AdapterView<?> adapterView,
								View view, int index, long rowId) {
								visitUrlInBrowser(searchResultItemList.get(index).get("url"));
						}
					});
		}
		
	}

	private void visitUrlInBrowser(String url) {
		Intent viewInBrowserIntent = new Intent(Intent.ACTION_VIEW); // Action to do.
		viewInBrowserIntent.setData(Uri.parse(url)); // Action-specific data.
		startActivity(viewInBrowserIntent);
	}
	
	/**
	 * Creates the options (bottom) menu.
	 * 
	 * Invoked only once per app start. 
	 * If you want to modify menu structure runtime
	 * you have to override onPrepareOptionsMenu() which is called every
	 * menu button press.
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.general_options_menu, menu);
	    return super.onCreateOptionsMenu(menu);
	    
	    // It's so simple but check searchTerm.setOnKeyListener() above.
	    // You have to be sure nothing swallows Menu key event.
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem menuItem) {
		
		switch (menuItem.getItemId()) {
			case R.id.menu_item_about:
				Intent aboutActivity = new Intent(this, About.class);
				startActivity(aboutActivity);
				break;
		}
		
		return super.onOptionsItemSelected(menuItem);
	}
	
	/**
	 * This method is invoked every time a context menu is about to show.
	 */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, view, menuInfo);
		if (view.getId() == R.id.searchResultList) {
			MenuInflater inflater = getMenuInflater();
			inflater.inflate(R.menu.searchresult_item_context_menu, menu);
			// Pulling exactly the same object which was passed to the list view adapter earlier.
			// Hence that brave typecast.
			AdapterContextMenuInfo contextMenuInfo = (AdapterContextMenuInfo) menuInfo;
			@SuppressWarnings("unchecked")
			Map<String, String> selectedMenuItem = (Map<String, String>) searchResultListView.getAdapter().getItem(contextMenuInfo.position);
			menu.setHeaderTitle(selectedMenuItem.get("visibleUrl"));
		}
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem menuItem) {
		Map<String, String> selectedMenuItem = null;
		
		switch (menuItem.getItemId()) {
			case R.id.menu_item_show_in_browser:
				selectedMenuItem = getSearchResultItemByMenuItem(menuItem);
				visitUrlInBrowser(selectedMenuItem.get("url"));
				break;
			case R.id.menu_item_send_url_in_email:
				selectedMenuItem = getSearchResultItemByMenuItem(menuItem);
				
				// Sending a message with intents is as easy as ABC
				Intent i = new Intent(Intent.ACTION_SEND);
				i.setType("plain/text");
				// Some extra but self-explanatory email data.
				i.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.emailSubjectPrefix) + selectedMenuItem.get("url"));
				i.putExtra(Intent.EXTRA_TEXT   , selectedMenuItem.get("url"));
				try {
				    startActivity(Intent.createChooser(i, getString(R.string.menu_item_send_url_in_email)));
				} catch (android.content.ActivityNotFoundException ex) {
				    handleGeneralError("There are no email clients installed.");
				}
				break;
		}
		return super.onContextItemSelected(menuItem);
		
		// If you try email sending in emulator it won't work 
		// till you configure the only available email client.
		// Go to Settings -> Accounts&Sync and add a new account.
		// If you have a gmail account use m.google.com as an exchange server.
	}

	private Map<String, String> getSearchResultItemByMenuItem(MenuItem menuItem) {
		AdapterContextMenuInfo contextMenuInfo = (AdapterContextMenuInfo) menuItem.getMenuInfo();
		@SuppressWarnings("unchecked")
		Map<String, String> selectedMenuItem = (Map<String, String>) searchResultListView.getAdapter().getItem(contextMenuInfo.position);

		return selectedMenuItem;
	}
	
	/**
	 * Simple error handling method for debugging purposes.
	 * 
	 * @param message
	 *            Error message.
	 */
	private void handleGeneralError(String message) {
		Log.e("General error", message);
		Toast.makeText(GoogleSearch.this, message, Toast.LENGTH_LONG).show();
	}
}