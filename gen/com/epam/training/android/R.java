/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.epam.training.android;

public final class R {
    public static final class attr {
    }
    public static final class drawable {
        public static final int androidlogo=0x7f020000;
        public static final int ic_launcher=0x7f020001;
    }
    public static final class id {
        public static final int aboutImage=0x7f060000;
        public static final int aboutText=0x7f060002;
        public static final int aboutTitle=0x7f060001;
        public static final int menu_item_about=0x7f060009;
        public static final int menu_item_send_url_in_email=0x7f06000b;
        public static final int menu_item_show_in_browser=0x7f06000a;
        public static final int searchButton=0x7f060004;
        public static final int searchResultItemContent=0x7f060008;
        public static final int searchResultItemTitle=0x7f060006;
        public static final int searchResultList=0x7f060005;
        public static final int searchResultVisibleUrl=0x7f060007;
        public static final int searchTerm=0x7f060003;
    }
    public static final class layout {
        public static final int about=0x7f030000;
        public static final int main=0x7f030001;
        public static final int search_result_item=0x7f030002;
    }
    public static final class menu {
        public static final int general_options_menu=0x7f050000;
        public static final int searchresult_item_context_menu=0x7f050001;
    }
    public static final class string {
        public static final int about_image_description=0x7f040002;
        /**  Look! The file name doesn't matter at all but the folder name. 
    Every other string what is not available for the current language come from the default
    /res/values folder. 
 Sorry if this is some kind of cursing or stuff. Translated by Google. 
         */
        public static final int about_text=0x7f040001;
        /**  It's not mandatory to separate menu strings. 
	This file was created just to demonstrate we can have multiple xml-s for strings. 
         */
        public static final int about_title=0x7f040000;
        public static final int app_name=0x7f040007;
        public static final int emailSubjectPrefix=0x7f04000d;
        public static final int emptySearchTermAlertDialogButtonText=0x7f04000b;
        public static final int emptySearchTermAlertDialogText=0x7f04000a;
        public static final int enter_search_term=0x7f040008;
        public static final int hello=0x7f040006;
        /**  It's not mandatory to separate menu strings. 
	This file was created just to demonstrate we can have multiple xml-s for strings. 
         */
        public static final int menu_item_about=0x7f040003;
        public static final int menu_item_send_url_in_email=0x7f040005;
        public static final int menu_item_show_in_browser=0x7f040004;
        public static final int search=0x7f040009;
        public static final int searchInProgressDialogTitle=0x7f04000c;
    }
}
